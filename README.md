# keycloak-spi
This repository contains some custom [Keycloak SPIs](https://www.keycloak.org/docs/latest/server_development/#_providers)
used by CSC on our Keycloak instance.

## Build
Requires OpenJDK 11+ and Maven 3.6+.
```sh
mvn clean package
```

## Install
Copy target/csc-keycloak-spi.jar to /opt/jboss/keycloak/standalone/deployments
in the container where Keycloak is running.
